

import time
from selenium import webdriver


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.maximize_window()
    driver.get("https://the-internet.herokuapp.com/javascript_alerts")
    print('inside setup')


def test_validate_on_jsalert():
    driver.find_element_by_xpath("(//button)[1]").click()
    alert = driver.switch_to.alert
    alert.accept()
    print("Result is: ", driver.find_element_by_id("result").text)


def test_validate_on_jsconfirm():
    driver.find_element_by_xpath("(//button)[2]").click()
    alert = driver.switch_to.alert
    alert.dismiss()
    print("Result is: ", driver.find_element_by_id("result").text)


def test_validate_on_jsprompt():
    driver.find_element_by_xpath("(//button)[3]").click()
    alert = driver.switch_to.alert
    alert.send_keys("testing")
    alert.accept()
    print("Result is: ", driver.find_element_by_id("result").text)


def teardown():
    time.sleep(2)
    driver.quit()
