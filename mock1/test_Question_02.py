
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    driver.maximize_window()


def test_wiki():
    # Step_1 : Open the URL in Chrome
    driver.get("http://www.wikipedia.org")

    # Step_2 : Print the No. Of English articles
    art = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/small/bdi")
    noofart = art.text
    print('No. of Articles are ', noofart)

    # Step_3 : Click on English Link
    driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong").click()

    # Step_4 : Type in 'Anna University' in the SearchBox and Press Enter Key
    webox = driver.find_element_by_xpath("//*[@id='searchInput'] ")
    webox.send_keys("Anna University")
    time.sleep(2)
    webox.send_keys(Keys.ENTER)

    # Step_5 : print Motto Text
    xpathmotto = "//*[@id='mw-content-text']/div/table[1]/tbody/tr[2]/td"
    mottotext = driver.find_element_by_xpath(xpathmotto).text
    print("Motto Text is : ", mottotext)

    # Step_6 : Validate if Motto Text contains the word 'Knowledge'
    expword = 'Knowledge'
    # if (mottotext.contains(expword)):   It is not a valid operation for str objects
    if expword in mottotext:
        print("Motto Text Contains :    ", expword)
    else:
        print("Motto Text DOES not Contains :    ", expword)


def teardown():
    time.sleep(2)
    driver.quit()
