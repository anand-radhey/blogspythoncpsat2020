# Qn2. Using PyTest / WebDriver script,
# •	open http://www.imdb.com/ in Google Chrome
# •	Search for movie “Gangs of New York” and
# •	open the movie details page and
# •	perform the following :
#   a.	Verify that the movie’s run time is less than 180 min.
#   b.	Verify that the movie’s genre contains “Crime”
#   c.	Verify that the MPAA rating of the movie is “R”
#   d.	Go to the User Reviews section of the movie and display the name of the Reviewer on the
#       PyCharm IDE console

import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)


def test_validate_movie_details():
    # Step 1 : Load the URL
    driver.get('http://www.imdb.com/')

    # Step 2 : Search for 'Gangs of New York'
    # //*[@id="suggestion-search"]
    searchbox = driver.find_element_by_xpath("//*[@id='suggestion-search']")
    searchbox.send_keys("Gangs of New York")
    searchbox.send_keys(Keys.ENTER)

    # Step 3 : Click on movie Link on search result page
    # //*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a
    driver.find_element_by_xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a").click()
    
    # Step 4 : Locate Quick Link Bar containing the below details required to validate
    #  R | 2h 47min | Crime, Drama | 20 December 2002 (USA)

    xpathqlink = "//*[@id=\"title-overview-widget\"]/div[1]/div[2]/div/div[2]/div[2]/div"
    textstr = driver.find_element_by_xpath(xpathqlink).text
    lst = textstr.split('|')
    rating = lst[0]
    runtime = lst[1]
    genres  = lst[2]
    print("Rating is  : ",rating)
    print("Runtime is  : ", runtime)
    print('Genres are : ', genres)

    # Step 5 : Validate Runtime
    rtlist = runtime.strip().split(' ')
    hr = rtlist[0].split('h')[0]
    min = rtlist[1].split('min')[0]
    print(' hr is : ', hr)
    print(' min is : ', min)

    intRunTime = int(hr) * 60 + int(min)
    print('Runtime in mins is : ', intRunTime)
    if intRunTime < 180:
        print('Runtime in mins is : ', intRunTime, ' AND it is less than 180 mins.')
    else:
        print('Runtime in mins is : ', intRunTime, ' AND it is NOT LESS THAN 180 mins.')

    # Step 6 : Validate Genre
    if 'Crime' in genres:
        print('Crime is found as the Genre of the movie ')
    else:
        print('Crime is NOT found as the Genre of the movie ')

    # Step 7 : Validate Rating
    if 'R' in rating:
        print('MPAA Rating of the movie is R ')
    else:
        print('MPAA Rating of the movie is NOT R but it is ', rating)

    # Step 8 : Click on User Reviews and print reviewer's name
    driver.find_element_by_xpath("//*[@id=\"quicklinksMainSection\"]/a[3]").click()
    str1 =  "//*[@id=\"main\"]/section/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/a"
    reviewer = driver.find_element_by_xpath(str1).text
    print("Reviewer of the Movie is : ", reviewer)
    

def teardown():
    time.sleep(2)
    driver.quit()
